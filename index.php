<?php
    $delta = @$_REQUEST['delta'];
    if(!is_numeric($delta) || !is_integer($delta + 0)) {
        $delta = 20;
    }
    
    function isSelected($val, $cur_val) {
        if($cur_val == $val) {
            echo "selected";
        } else {
            echo "";
        }
    }
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
       "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <title>Preisverteilung</title>
</head>
<body>
	<h1>Preisverteilung von Geschenketisch</h1>

        <img src="prices.php?d=<?=$delta?>" />
        
        <p>
            <form method="POST" action="index.php">
                Schrittweite: 
                <select name="delta" size="1" onchange="this.form.submit()">
                    <option value="5" <?=isSelected(5,$delta)?>>5 Euro</option>
                    <option value="10" <?=isSelected(10,$delta)?>>10 Euro</option>
                    <option value="20" <?=isSelected(20,$delta)?>>20 Euro</option>
                    <option value="40" <?=isSelected(40,$delta)?>>40 Euro</option>
                    <option value="60" <?=isSelected(60,$delta)?>>60 Euro</option>
                </select>
                <noscript><input type="submit" value="OK"></noscript>
            </form>
        </p>
</body>
</html>
