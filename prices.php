<?php // content="text/plain; charset=utf-8"
require_once ('jpgraph/jpgraph.php');
require_once ('jpgraph/jpgraph_bar.php');

function out($dat,$title="") {
    echo "<h3>$title</h3>\n";
    echo "<pre>\n";
    print_r($dat);
    echo "</pre>\n";
}

function getPrices() {
	$url = 'http://www.wishsite.de/wishlist/APPENDYOURWISHIDHERE';
	$html = @file_get_contents($url);
	$doc = @new DOMDocument();
	@$doc->loadHTML($html);
	$xml = @simplexml_import_dom($doc);
	$test = $xml->body;

	$prices = array();
	foreach ($xml->xpath("//div[@class='item']/table") as $item) {

		$price = $item->tr[1]->td->p;
		$parts = explode(' ',$price);
		$prices[] = $parts[0];
	}

	foreach($prices as &$price){
		if (strpos($price,'-') !== false) {
		    	$two_prices = explode('-',$price);
			$price = ($two_prices[0] + $two_prices[1]) / 2.0;
		}
	}
        
	return $prices;
	
}

function getDistrubtion($data, $delta = 20) {
    $maxraw = max($data);
    $max = $maxraw + $delta - (($maxraw + $delta) % $delta);
    $max_index = $max / $delta;
    $dist = array();
    $dist['values'] = array();
    $dist['labels'] = array();
    for($i=0; $i<$max_index; $i++) {
        $dist['values'][] = 0;
        $cur_t0 = $i * $delta;
        $cur_t1 = ($i + 1) * $delta;
        foreach($data as $dat) {
            if($dat >= $cur_t0 && $dat < $cur_t1) {
                $dist['values'][count($dist['values'])-1]++;
            }
        }
        $dist['labels'][] = "$cur_t0\nbis\n".($cur_t1 - 1);
    }
    return $dist;
}



function plot_bargraph($y,$labels) {
    // Create the graph. 
    $graph = new Graph(900,300,'auto');
    $graph->SetScale("textlin");
    $graph->img->SetMargin(60,30,20,80);
    $graph->yaxis->SetTitleMargin(45);
    $graph->yaxis->scale->SetGrace(30);

    // Labels
    
    if(count($y) > 35) {
        foreach($labels as &$label) {
            $label = "";
        }
    } else {
        $graph->xaxis->SetTitleMargin(45);
    }
    
    $graph->xaxis->SetTickLabels($labels);
    

    // Create a bar pot
    $bplot = new BarPlot($y);

    $graph->Add($bplot);

    $graph->title->Set("Preisverteilung von ".array_sum($y)." Geschenken");
    $graph->xaxis->title->Set("Preis der Geschenke [Euro]");
    $graph->yaxis->title->Set("Anzahl der Geschenke");

    $graph->title->SetFont(FF_FONT1,FS_BOLD);
    $graph->yaxis->title->SetFont(FF_FONT1,FS_BOLD);
    $graph->xaxis->title->SetFont(FF_FONT1,FS_BOLD);

    $bplot->SetFillColor('red');

    $bplot->value->Show(); 
    $bplot->value->SetAngle(45);
    $bplot->value->SetFormat('%001d');
    $bplot->SetShadow();

    $bplot->SetFillGradient("#9d70d0","#411b6b",GRAD_HOR);

    $graph->Stroke();
}

$delta = @$_REQUEST['d'];
if(!is_numeric($delta) || !is_integer($delta + 0)) {
    $delta = 20;
}

$data = getDistrubtion(getPrices(),$delta);
plot_bargraph($data['values'],$data['labels']);



?>
